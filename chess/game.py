import pygame
from .board import Board
from .constants import BLACK, WHITE, SQUARE_SIZE, BLUE, RED, ROWS, COLS
from .piece import Piece
from copy import deepcopy
class Game:
    def __init__(self, win):
        self.win = win
        self._init()
        
    def _init(self):
        self.selected_row = None
        self.selected_col = None
        self.board = Board()
        self.oldBoards = []
        self.turn = WHITE
        self.valid_moves_for_selected_piece = {}
        self.update()

    def update(self):
        self.board.draw(self.win)
        if self.selected_row is not None and self.selected_col is not None:
            self.draw_valid_moves(self.valid_moves_for_selected_piece)
        kingInCheck = self.board.kingInCheck(self.turn)
        if kingInCheck[0]:
            pygame.draw.circle(self.win, RED, (kingInCheck[2] * SQUARE_SIZE + SQUARE_SIZE // 2, kingInCheck[1] * SQUARE_SIZE + SQUARE_SIZE  // 2), 15)

        pygame.display.update()

    def reset(self):
        self._init()

    def select(self, row, col):
        if self.selected_row is not None and self.selected_col is not None:
            result = self._move(row, col)
            if not result:
                self.selected_row = None
                self.selected_col = None
                # self.select(row, col)
        piece = self.board.get_piece(row, col)
        if piece != 0 and Piece.get_color(piece) == self.turn:
            self.selected_row = row
            self.selected_col = col
            self.valid_moves_for_selected_piece = Piece.get_valid_moves(piece, row, col, self.board)
            return True
        return False

    def undo(self):
        if self.oldBoards.__len__() > 0:
            self.board = self.oldBoards.pop()
            self.change_turn()
            self.update()

    def _move(self, row, col):
        if self.selected_row is not None and self.selected_col is not None and (row, col) in self.valid_moves_for_selected_piece:
            self.oldBoards.append(self.board)
            self.board = deepcopy(self.board)
            self.board.move(self.selected_row, self.selected_col, row, col)
            if row == 0 and self.board.board[row][col] == 1:
                myinput = 0
                while myinput != 9 and not(5 >= myinput >= 3):
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_q:
                                myinput = 9
                            elif event.key == pygame.K_r:
                                myinput = 5
                            elif event.key == pygame.K_b:
                                myinput = 4
                            elif event.key == pygame.K_k:
                                myinput = 3
                self.board.board[row][col] = myinput
            elif row == ROWS - 1 and self.board.board[row][col] == -1:
                myinput = 0
                while myinput != 9 and not(5 >= myinput >= 3):
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_q:
                                myinput = 9
                            elif event.key == pygame.K_r:
                                myinput = 5
                            elif event.key == pygame.K_b:
                                myinput = 4
                            elif event.key == pygame.K_k:
                                myinput = 3
                self.board.board[row][col] = -myinput
            self.change_turn()
        else:
            return False
        return True
    
    def change_turn(self):
        self.valid_moves_for_selected_piece = {}
        self.selected_row = None
        self.selected_col = None
        if self.turn == BLACK:
            self.turn = WHITE
        else:
            self.turn = BLACK

    def draw_valid_moves(self, moves):
        for move in moves:
            row, col = move
            pygame.draw.circle(self.win, BLUE, (col * SQUARE_SIZE + SQUARE_SIZE // 2, row * SQUARE_SIZE + SQUARE_SIZE  // 2), 15)
    
    def winner(self):
        return self.board.winner(self.turn)

    def get_board(self):
        return self.board

    def ai_move(self, board):
        self.board = board
        self.change_turn()


