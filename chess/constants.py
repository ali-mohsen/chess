import pygame

WIDTH, HEIGHT = 800, 800
ROWS, COLS = 8, 8
SQUARE_SIZE = WIDTH//COLS

#RGB
RED = (255, 0, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREY = (128, 128, 128)


wb = pygame.transform.scale(pygame.image.load('assets/test/B.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
wk = pygame.transform.scale(pygame.image.load('assets/test/K.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
wn = pygame.transform.scale(pygame.image.load('assets/test/N.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
wp = pygame.transform.scale(pygame.image.load('assets/test/P.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
wq = pygame.transform.scale(pygame.image.load('assets/test/Q.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
wr = pygame.transform.scale(pygame.image.load('assets/test/R.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))

bb = pygame.transform.scale(pygame.image.load('assets/test/b.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
bk = pygame.transform.scale(pygame.image.load('assets/test/k.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
bn = pygame.transform.scale(pygame.image.load('assets/test/n.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
bp = pygame.transform.scale(pygame.image.load('assets/test/p.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
bq = pygame.transform.scale(pygame.image.load('assets/test/q.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))
br = pygame.transform.scale(pygame.image.load('assets/test/r.png'), (SQUARE_SIZE * 0.7, SQUARE_SIZE * 0.7))


square_dark = pygame.transform.scale(pygame.image.load('assets/test/square_brown_dark_1x.png'), (SQUARE_SIZE, SQUARE_SIZE))
square_light = pygame.transform.scale(pygame.image.load('assets/test/square_brown_light_1x.png'), (SQUARE_SIZE, SQUARE_SIZE))




