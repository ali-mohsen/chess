import pygame
from .constants import ROWS, COLS, wp, wn, wb, wr, wq, wk, bp, bn, bb, br, bq, bk, SQUARE_SIZE, WHITE, BLACK

from itertools import count
from copy import deepcopy

class Piece:

    @staticmethod
    def draw(win, p, row, col):
        if p == 1:
            piece = wp
        elif p == 3:
            piece = wn
        elif p == 4:
            piece = wb
        elif p == 5:
            piece = wr
        elif p == 9:
            piece = wq
        elif p == float('+inf'):
            piece = wk

        elif p == -1:
            piece = bp
        elif p == -3:
            piece = bn
        elif p == -4:
            piece = bb
        elif p == -5:
            piece = br
        elif p == -9:
            piece = bq
        elif p == float('-inf'):
            piece = bk

        else:
            piece = None

        x = SQUARE_SIZE * col + SQUARE_SIZE // 2
        y = SQUARE_SIZE * row + SQUARE_SIZE // 2
        if piece:
            win.blit(piece, (x - piece.get_width()//2, y - piece.get_height()//2))

    @staticmethod
    def get_color(p):
        if p < 0:
            return BLACK
        elif p > 0:
            return WHITE
        return 0

    @staticmethod
    def get_valid_moves(p, row, col, board, internal = False):
        moves = {}
        if p == 1 or p == -1:
            if p == 1:
                if board.get_piece(row-1, col) == 0:
                    moves[(row - 1 ,col)] = 1
                if row == ROWS -2 and board.get_piece(row-2, col) == 0 and board.get_piece(row-1, col) == 0:
                    moves[(row - 2 ,col)] = 1
                if board.get_piece(row-1,col+1) < 0:
                    moves[(row - 1 ,col+1)] = 1
                if board.get_piece(row-1,col-1) < 0:
                    moves[(row - 1 ,col-1)] = 1
            elif p == -1:
                if board.get_piece(row+1, col) == 0:
                    moves[(row + 1 ,col)] = 1
                if row == 1 and board.get_piece(row+2, col) == 0 and board.get_piece(row+1, col) == 0:
                    moves[(row + 2 ,col)] = 1
                if board.get_piece(row+1,col+1) > 0:
                    moves[(row + 1 ,col+1)] = 1
                if board.get_piece(row+1,col-1) > 0:
                    moves[(row + 1 ,col-1)] = 1
        elif p == 3 or p == -3:
            for x in (-1, 1):
                for y in (-2, 2):
                    if 0 <= row+x < ROWS and 0 <= col+y < COLS:
                        moves[(row+x, col+y)] = 1
            for x in (-2, 2):
                for y in (-1, 1):
                    if 0 <= row+x < ROWS and 0 <= col+y < COLS:
                        moves[(row+x, col+y)] = 1
        elif p == 4 or p == -4:
            for dx in (-1, 1):
                for dy in (-1, 1):
                    for i in count(start=1):
                        newx = row + dx*i
                        newy = col + dy*i
                        if 0 <= newx < 8 and 0 <= newy < 8:
                             moves[(newx ,newy)] = 1
                        else:
                            break
                        if board.get_piece(newx ,newy) != 0:
                            break
        elif p == 5 or p == -5:
            for dx in (-1, 1):
                for dy in (-1, 1):
                    for i in count(start=1):
                        newx = row + dx*i
                        if 0 <= newx < 8:
                            moves[(newx ,col)] = 1
                        else:
                            break
                        if board.get_piece(newx ,col) != 0:
                            break
                    for i in count(start=1):
                        newy = col + dy*i
                        if 0 <= newy < 8:
                            moves[(row, newy)] = 1
                        else:
                            break
                        if board.get_piece(row ,newy) != 0:
                            break
        elif p == 9 or p == -9:
            for dx in (-1, 1):
                for dy in (-1, 1):
                    for i in count(start=1):
                        newx = row + dx*i
                        if 0 <= newx < 8:
                            moves[(newx ,col)] = 1
                        else:
                            break
                        if board.get_piece(newx ,col) != 0:
                            break
                    for i in count(start=1):
                        newy = col + dy*i
                        if 0 <= newy < 8:
                            moves[(row, newy)] = 1
                        else:
                            break
                        if board.get_piece(row ,newy) != 0:
                            break


            for dx in (-1, 1):
                for dy in (-1, 1):
                    for i in count(start=1):
                        newx = row + dx*i
                        newy = col + dy*i
                        if 0 <= newx < 8 and 0 <= newy < 8:
                             moves[(newx ,newy)] = 1
                        else:
                            break
                        if board.get_piece(newx ,newy) != 0:
                            break
        elif p == float('+inf') or p == float('-inf'):
            for x in range(row-1, row+2):
                for y in range(col-1, col+2):
                    if 0 <= x < ROWS and 0 <= y < COLS and not(x == row and y == col):
                        moves[(x , y)] = 1
            
        for k in list(moves.keys()):
            x,y = k
            if p > 0 and board.get_piece(x,y) > 0:
                del moves[k]
            elif p < 0 and board.get_piece(x,y) < 0:
                del moves[k]

        if not internal:
            if board.board[row][col] > 0:
                color = WHITE
            elif board.board[row][col] < 0:
                color = BLACK
            for k in list(moves.keys()):
                x,y = k
                temp_board = deepcopy(board)
                temp_board.move(row, col, x, y)
                kingincheck = temp_board.kingInCheck(color)
                if kingincheck[0]:
                    del moves[k]

        return moves

    @staticmethod
    def king_in_check(pieces, row, col, board):
        for p in pieces:
            moves = Piece.get_valid_moves(p[0], p[1], p[2], board, True)
            for move in moves:
                if move == (row, col):
                    return (True, row, col)
        return (False, row, col)
