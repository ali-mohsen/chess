import pygame
from .piece import Piece
from .constants import BLACK, WHITE, ROWS, COLS, SQUARE_SIZE, square_dark, square_light, RED

class Board:
    # **White**
    # 9 = white queen
    # 5 = white rook
    # 4 = bishop
    # 3 = knight
    # 1 = pawn
    # **black**
    # -9 = white queen
    # -5 = white rook
    # -4 = bishop
    # -3 = knight
    # -1 = pawn
    # White King: very large positive number
    # Black King: very large negative number

    def __init__(self):
        self.board = []
        self.create_boared()

    def create_boared(self):
        # 8| -5 -3 -4 -9 -inf -4 -3 -5
        # 7| -1 -1 -1 -1 -1 -1 -1 -1
        # 6|  0  0  0  0  0  0  0  0
        # 5|  0  0  0  0  0  0  0  0
        # 4|  0  0  0  0  0  0  0  0
        # 3|  0  0  0  0  0  0  0  0
        # 2|  1  1  1  1  1  1  1  1 
        # 1|  5  3  4  9  inf  4  3  5
        #   -------------------------
        #    1  2  3  4  5  6  7  8
        self.board = [
                    [-5, -3, -4, -9, float('-inf'), -4, -3, -5],
                    [-1, -1, -1, -1, -1, -1, -1, -1],
                    [0,  0,  0,  0,  0,  0,  0,  0],
                    [0,  0,  0,  0,  0,  0,  0,  0],
                    [0,  0,  0,  0,  0,  0,  0,  0],
                    [0,  0,  0,  0,  0,  0,  0,  0],
                    [1, 1, 1, 1, 1, 1, 1, 1],
                    [5, 3, 4, 9, float('+inf'), 4, 3, 5]
                ]

    def draw_squares(self, win):
        win.fill(BLACK)
        for row in range(ROWS):
            for col in range(row % 2 - 1, COLS, 2):
                win.blit(square_dark, (row * SQUARE_SIZE, col * SQUARE_SIZE))
            for col in range(row % 2, COLS, 2):
                win.blit(square_light, (row * SQUARE_SIZE, col * SQUARE_SIZE))
                # pygame.draw.rect(win, WHITE, (row * SQUARE_SIZE, col * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))

    def move(self, old_row, old_col, new_row, new_col):
        self.board[new_row][new_col] = self.board[old_row][old_col]
        self.board[old_row][old_col] = 0
        # if new_row == 0 and self.board[new_row][new_col] == 1:
        #     self.board[new_row][new_col] = 9
        # elif new_row == ROWS - 1 and self.board[new_row][new_col] == -1:
        #     self.board[new_row][new_col] = -9

    def get_piece(self, row, col):
        if row >= ROWS or row < 0 or col >= COLS or col < 0:
            return 0
        return self.board[row][col]

    def draw(self, win):
        self.draw_squares(win)
        for row in range(ROWS):
            for col in range(COLS):
                piece = self.board[row][col]
                if piece != 0:
                    Piece.draw(win, piece, row, col)
    
    def get_all_pieces(self, color):
        pieces = []
        for row in range(ROWS):
            for col in range(COLS):
                piece = self.board[row][col]
                if color == BLACK and piece < 0:
                    pieces.append((piece, row, col))
                if color == WHITE and piece > 0:
                    pieces.append((piece, row, col))
        return pieces


    def kingInCheck(self, color):
        king = None
        if color == BLACK:
            for row in range(ROWS):
                for col in range(COLS):
                    piece = self.board[row][col]
                    if piece == float('-inf'):
                        king = (row, col)
                        break
                if king:
                    break
            return Piece.king_in_check(self.get_all_pieces(WHITE), king[0], king[1], self)
        else:
            for row in range(ROWS):
                for col in range(COLS):
                    piece = self.board[row][col]
                    if piece == float('+inf'):
                        king = (row, col)
                        break
                if king:
                    break
            return Piece.king_in_check(self.get_all_pieces(BLACK), king[0], king[1], self)

    def winner(self, color):
        kingInCheck = self.kingInCheck(color)
        allPieces = self.get_all_pieces(color)
        allMoves = []
        for piece in allPieces:
            moves = Piece.get_valid_moves(piece[0], piece[1], piece[2], self)
            if(moves):
                allMoves.append(moves)
        if not allMoves:
            if kingInCheck[0]:
                if color == WHITE:
                    return 'black is the winner'
                else:
                    return 'white is the winner'
            else:
                pass
        return 'noWinner'

    def evaluate(self):
        # TODO: find a way to evaluate the situation
        return 0