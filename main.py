#!/usr/bin/env python

import pygame
from chess.constants import WIDTH, HEIGHT, WHITE, SQUARE_SIZE
from chess.game import Game

FPS = 15

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('chess')

def get_row_col_from_mouse(pos):
    x, y = pos
    row = y // SQUARE_SIZE
    col = x // SQUARE_SIZE
    return row, col

def main():
    run = True
    clock = pygame.time.Clock()
    game = Game(WIN)
    while run:
        clock.tick(FPS)

        # if game.turn == WHITE:
        #     value, new_board = minimax(game.get_board(), 4, WHITE, game)
        #     game.ai_move(new_board)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            elif event.type == pygame.KEYDOWN:
                mods = pygame.key.get_mods()
                if mods and pygame.KMOD_CTRL and event.key == pygame.K_z:
                    game.undo()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                row, col = get_row_col_from_mouse(pos)
                game.select(row, col)
                game.update()

        if game.winner() != 'noWinner':
            print(game.winner())
            game.reset()

    pygame.quit()
main()


#TODO: casteling
#TODO: pone eatig (التعدي)
#TODO: draw cases (التعادل)

#TODO: better upgrading pones input from GUI

#TODO: AI
